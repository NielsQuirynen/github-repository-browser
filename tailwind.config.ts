module.exports = {
  mode: 'jit',
  purge: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './app.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        primary: {
          DEFAULT: '#1E00FF',
          '50': '#fafaff',
          '100': '#AEA3FF',
          '200': '#8A7AFF',
          '300': '#6652FF',
          '400': '#4229FF',
          '500': '#1E00FF',
          '600': '#1700C7',
          '700': '#11008F',
          '800': '#0A0057',
          '900': '#04001F'
        },
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}

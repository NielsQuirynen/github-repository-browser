import { ref, unref, Ref } from 'vue';
import { Sort, PaginatedResponse, ApiResponse, DatatableColumn, SortDirection } from '~/types'
import { initialPageInfo, isErrorsResponse } from '~/utils/helpers'

export type FetchPaginationResult<T= any> = ({ cursor, sort }: {
  cursor: string | null;
  sort: Sort;
}) => Promise<ApiResponse<PaginatedResponse<T>>>;

export type Props<T = any> = {
  defaultSort?: Sort;
  fetchPaginationResult: FetchPaginationResult<T>;
}

export default <T = any>({ fetchPaginationResult, defaultSort }: Props<T>) => {
  const sort = ref(defaultSort || { field: undefined, direction: undefined });
  const error = ref('');
  const items = ref<T[]>([]) as Ref<T[]>;
  const pageInfo = ref(initialPageInfo);
  const totalCount = ref(0);
  const isLoading = ref(false);
  const isInitialized = ref(false);

  const fetchItems = async (isLoadMore = false) => {
    error.value = '';
    isLoading.value = true;

    const data = await fetchPaginationResult({
      cursor: isLoadMore ? pageInfo.value.endCursor : null,
      sort: sort.value,
    });

    isInitialized.value = true;
    isLoading.value = false;

    if (!data || isErrorsResponse(data)) {
      error.value = data?.errors[0].message;
      pageInfo.value = initialPageInfo;
      items.value = [];
      totalCount.value = 0;
      return;
    }

    pageInfo.value = data.pageInfo;
    totalCount.value = data.totalCount;
    items.value = isLoadMore ? [...items.value, ...data.items] : data.items;
  }

  const toggleSort = (column: DatatableColumn) => {
    if (!column.sortable) {
      return;
    }

    const updateSort: Sort = unref(sort);

    const id = column.id;
    if (id === updateSort.field) {
      switch (updateSort.direction) {
        case SortDirection.ASC:
          updateSort.direction = SortDirection.DESC;
          break;
        case SortDirection.DESC:
          updateSort.direction = undefined;
          break;
        default:
          updateSort.direction = SortDirection.ASC;
          break;
      }
    } else {
      updateSort.direction = SortDirection.ASC;
    }

    updateSort.field = updateSort.direction ? id : undefined;
    sort.value = updateSort;
    fetchItems();
  };

  const loadMore = () => {
    if (pageInfo.value.hasNextPage) {
      fetchItems(true);
    }
  }

  return {
    sort,
    items,
    totalCount,
    error,
    isInitialized,
    isLoading,
    pageInfo,
    loadMore,
    toggleSort,
    fetchItems,
  };
};

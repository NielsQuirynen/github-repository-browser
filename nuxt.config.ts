import { defineNuxtConfig } from 'nuxt3'

export default defineNuxtConfig({
  env: {
    baseUrl: process.env.BASE_URL || 'http://localhost:3000'
  },
  typescript: {
    strict: true
  },
  meta: {
    title: 'Github Repository Browser',
  },
  buildModules: [
    '@vueuse/core/nuxt',
    // '@nuxtjs/tailwindcss',
    '@pinia/nuxt',
  ],
  build: {
    postcss: {
      postcssOptions: {
        plugins: {
          'postcss-nested': {},
          tailwindcss: require('./tailwind.config'),
          autoprefixer: {},
        }
      }
    },
  },
  // unocss: {
  //   shortcuts: [
  //     ['c-button', 'px-4 py-1 rounded inline-block bg-teal-600 text-white cursor-pointer hover:bg-teal-700 disabled:cursor-default disabled:bg-gray-600 disabled:opacity-50'],
  //   ],
  //   uno: true,
  //   attributify: true,
  //   icons: {
  //     scale: 1.2,
  //   },
  // },
})

import { acceptHMRUpdate, defineStore } from 'pinia'

export const useRepositoryStore = defineStore('repository', () => {
  const searchUsername = ref('vuejs');

  function setSearchUsername(value: string) {
    searchUsername.value = value;
  }

  return {
    searchUsername,
    setSearchUsername,
  }
})

if (import.meta.hot)
  import.meta.hot.accept(acceptHMRUpdate(useRepositoryStore, import.meta.hot))

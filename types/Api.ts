import { CommitPreview, RepositoryPreview } from '~/types'

export type ApiError = { message: string };

export type ApiErrorsResponse = {
  errors: ApiError[];
};

export type ApiResponse<T> = T | ApiErrorsResponse;

export type PageInfo = {
  startCursor: string | null;
  endCursor: string | null;
  hasNextPage: boolean;
  hasPreviousPage: boolean;
};

export type GithubRepositoryOwnerResponse = {
  data: {
    repositoryOwner?: {
      repositories: {
        nodes: RepositoryPreview[];
        pageInfo: PageInfo;
        totalCount: number;
      },
    }
  }
};

export type GithubRepositoryCommitsResponse = {
  data: {
    repository?: {
      defaultBranchRef?: {
        target?: {
          history: {
            nodes: CommitPreview[];
            pageInfo: PageInfo;
            totalCount: number;
          }
        }
      }
    }
  }
};

export type PaginatedResponse<T = any> = {
  items: T[];
  pageInfo: PageInfo;
  totalCount: number;
};

export type PaginatedRepositoriesResponse = PaginatedResponse<RepositoryPreview>;

export type PaginatedCommitsResponse = PaginatedResponse<CommitPreview>;


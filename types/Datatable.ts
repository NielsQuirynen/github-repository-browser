export enum SortDirection {
  ASC = 'ASC',
  DESC = 'DESC',
}

export type Sort<Field = string> = {
  field?: Field;
  direction?: SortDirection;
}

export type DatatableFilter = {
  title?: string;
  options: {
    value: any;
    label: string;
  }[];
}

export type DatatableColumn = {
  id: string;
  title: string;
  sortable: boolean;
}

export type DatatableProps<T = any> = {
  data: T[];
  columns: DatatableColumn[];
  isLoading?: boolean;
  isInitialized?: boolean;
  loadMore?: () => void;
}

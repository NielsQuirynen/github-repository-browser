export type CommitPreview = {
  id: string;
  message: string;
  committedDate: string;
  author?: {
    name: string;
  }
}

export enum CommitField {
  MESSAGE = 'MESSAGE',
  AUTHOR = 'AUTHOR',
  DATE = 'DATE',
}

export type RepositoryPreview = {
  name: string;
  id: string;
  stargazerCount: number;
  createdAt: string;
  primaryLanguage?: {
    name: string;
  };
  owner: {
    login: string;
  }
}

export enum RepositoryField {
  NAME = 'NAME',
  STARGAZERS = 'STARGAZERS',
  LANGUAGE = 'LANGUAGE',
  CREATED_AT = 'CREATED_AT',
}

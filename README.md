# Github Repository Browser

## Features
- Search repositories by username
- Sort repositories by name / stars / date
- Fetch additional entries on scroll
- View commit history of repositories
- Search through loaded commits
- SSR

## Tech stack

Built with [Vitesse for Nuxt 3](https://github.com/antfu/vitesse-nuxt3) starter template by [
Anthony Fu](https://github.com/antfu/)  

- [💚 Nuxt 3](https://v3.nuxtjs.org) - SSR, ESR, File-based routing, components auto importing, modules, etc.

- ⚡️ Vite - Instant HMR

- 🎨 [TailwindCSS](https://github.com/tailwindlabs/tailwindcss) - A utility-first CSS framework

- 🔥 The `<script setup>` syntax

- 🍍 [State Management via Pinia](https://pinia.esm.dev/)

- 📥 APIs auto importing - for Composition API, VueUse and custom composables.

- 🦾 TypeScript


## Get project up and running

- Clone the `.env.example` file and rename to`.env`. 
  
- Open `.env` file and replace the value of `VITE_GITHUB_ACCESS_TOKEN` with your personal Github API token

- Open terminal and `cd` to the repository root folder
  
- Install packages using `npm install`

- Run `npm run dev` and visit the local url in your browser

import { ApiResponse } from '~/types'

export const githubGraphqlFetch = <T = any>({ query, variables }: { query: string, variables: Record<string, any> }) => {
  return $fetch<ApiResponse<T>>(process.env.VITE_GITHUB_GRAPHQL_URL || '', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `bearer ${process.env.VITE_GITHUB_ACCESS_TOKEN}`,
    },
    body: JSON.stringify({
      query,
      variables,
    }),
  });
}

import type { IncomingMessage, ServerResponse } from 'http'
import {
  PaginatedRepositoriesResponse,
  GithubRepositoryOwnerResponse,
  ApiResponse,
  RepositoryField,
  SortDirection,
} from '~/types'
import { isErrorsResponse } from '~/utils/helpers/api'
import translate from '~/utils/helpers/translate'
import { githubGraphqlFetch } from '~/server/utils/helpers/api'

const query = `
  query RepositoriesByUsername($username: String!, $limit: Int!, $cursor: String, $orderByField: RepositoryOrderField!, $orderByDirection: OrderDirection!) {
    repositoryOwner(login: $username) {
      repositories(first: $limit, after: $cursor, orderBy: { field: $orderByField, direction: $orderByDirection }) {
        nodes {
          id
          name
          stargazerCount
          createdAt
          primaryLanguage {
            name
          }
          owner {
            login
          }
        }
        pageInfo {
          startCursor
          endCursor
          hasNextPage
          hasPreviousPage
        },
        totalCount
      }
    }
  }
`

export default async (req: IncomingMessage, res: ServerResponse): Promise<ApiResponse<PaginatedRepositoriesResponse>> => {
  const errorResponse = (message: string = translate('api.errors.default'), statusCode: number = 500) => {
    res.statusCode = statusCode;

    return {
      errors: [
        { message }
      ]
    }
  }

  try {
    const baseUrl = `https://${req.headers.host}/`;
    const queryObject = new URL(req.url || '', baseUrl);
    const username = queryObject.searchParams.get('username');
    const cursor = queryObject.searchParams.get('cursor') || null;
    const orderByField = queryObject.searchParams.get('orderByField') || RepositoryField.STARGAZERS;
    const orderByDirection = queryObject.searchParams.get('orderByDirection') || SortDirection.DESC;

    if (!username) {
      return errorResponse(translate('api.errors.invalid_username'), 403);
    }

    const variables = {
      username,
      cursor,
      orderByField,
      orderByDirection,
      limit: 20,
    }
    const response = await githubGraphqlFetch<GithubRepositoryOwnerResponse>({ query, variables })

    if (!response || isErrorsResponse(response)) {
      throw Error();
    }

    if (!response.data.repositoryOwner) {
      return errorResponse(translate('api.errors.user_not_found'), 404);
    }

    const { nodes, pageInfo, totalCount } = response.data.repositoryOwner.repositories;

    return {
      items: nodes,
      pageInfo,
      totalCount,
    }
  } catch(error) {
    return errorResponse();
  }
}

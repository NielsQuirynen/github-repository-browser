import type { IncomingMessage, ServerResponse } from 'http'
import {
  PaginatedCommitsResponse,
  ApiResponse,
  GithubRepositoryCommitsResponse,
} from '~/types'
import { isErrorsResponse } from '~/utils/helpers/api'
import translate from '~/utils/helpers/translate'
import { githubGraphqlFetch } from '~/server/utils/helpers/api'

const query = `
  query RepositoryCommitHistory($owner: String!, $name: String!, $limit: Int!, $cursor: String) {
    repository(name: $name, owner: $owner) {
      defaultBranchRef {
        target {
          ... on Commit {
            history(first: $limit, after: $cursor) {
              nodes {
                message
                committedDate
                author {
                  name
                }
              }
              pageInfo {
                startCursor
                endCursor
                hasNextPage
                hasPreviousPage
              },
              totalCount
            }
          }
        }
      }
    }
  }
`

export default async (req: IncomingMessage, res: ServerResponse): Promise<ApiResponse<PaginatedCommitsResponse>> => {
  const errorResponse = (message: string = translate('api.errors.default'), statusCode: number = 500) => {
    res.statusCode = statusCode;

    return {
      errors: [
        { message }
      ]
    }
  }

  try {
    const baseUrl = `https://${req.headers.host}/`;
    const queryObject = new URL(req.url || '', baseUrl);
    const owner = queryObject.searchParams.get('owner') || null;
    const name = queryObject.searchParams.get('name') || null;
    const cursor = queryObject.searchParams.get('cursor') || null;

    if (!owner || !name) {
      return errorResponse(translate('api.errors.repository_not_found'), 403);
    }

    const variables = {
      owner,
      name,
      cursor,
      limit: 20,
    }
    const response = await githubGraphqlFetch<GithubRepositoryCommitsResponse>({ query, variables })

    const maybeNotFoundResponse = response as GithubRepositoryCommitsResponse;
    if (maybeNotFoundResponse?.data && !maybeNotFoundResponse.data.repository) {
      return errorResponse(translate('api.errors.repository_not_found'), 404);
    }

    if (!response || isErrorsResponse(response)) {
      throw Error();
    }

    if (!response.data.repository?.defaultBranchRef?.target?.history) {
      return errorResponse(translate('api.errors.repository_commmits_not_found'), 404);
    }

    const { nodes, pageInfo, totalCount } = response.data.repository.defaultBranchRef.target.history;

    return {
      items: nodes,
      pageInfo,
      totalCount,
    }
  } catch(error) {
    return errorResponse();
  }
}

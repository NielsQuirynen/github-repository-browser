import translations from '~/utils/locales';
import { get } from 'lodash-es';
const locale = 'en';

// todo: replace with translation plugin when needed
const translate = (key: string) => get(translations, `${locale}.${key}`) || key;

export default translate;

import { PageInfo } from '~/types'

export const initialPageInfo: PageInfo = {
  startCursor: null,
  endCursor: null,
  hasNextPage: false,
  hasPreviousPage: false,
}

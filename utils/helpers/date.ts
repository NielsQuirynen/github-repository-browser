import dayjs from 'dayjs';

export const formatDate = (date: string, format = 'DD/MM/YYYY') => {
  return dayjs(date).format(format);
}

export const formatDateTime = (date: string) => {
  return formatDate(date, 'DD/MM/YYYY - HH:mm:ss');
}

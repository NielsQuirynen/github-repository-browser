import { ApiErrorsResponse } from '~/types'

/**
 * Helper function for filtering API error responses
 */
export const isErrorsResponse = <T>(data: T | ApiErrorsResponse): data is ApiErrorsResponse => {
  return (data as ApiErrorsResponse).errors !== undefined;
};

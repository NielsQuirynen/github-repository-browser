export default {
  search: {
    fields: {
      username: {
        label: 'Username',
        placeholder: 'Username',
      },
    },
    total: '{count} repositories found for user "{username}"',
    submit_button: 'Find repositories',
  },
  columns: {
    name: 'Name',
    stars: 'Stars',
    language: 'Language',
    created_at: 'Created at',
  },
  return_to_repositories: 'Return to repositories'
}

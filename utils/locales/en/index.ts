import api from './api';
import commit from './commit';
import datatable from './datatable';
import repository from './repository';

export default {
  api,
  commit,
  datatable,
  repository,
}

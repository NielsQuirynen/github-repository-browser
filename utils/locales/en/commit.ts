export default {
  search: {
    fields: {
      term: {
        label: 'Search',
        placeholder: 'Search commit history',
      },
    },
    total: '{count} commits found for repository "{repository}"',
    submit_button: 'Find commits',
  },
  columns: {
    message: 'Message',
    author: 'Author',
    date: 'Date',
  },
}

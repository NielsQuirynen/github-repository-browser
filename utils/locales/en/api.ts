export default {
  errors: {
    invalid_username: 'Please provide a valid username',
    user_not_found: 'Could not find user',
    repository_not_found: 'Could not find repository',
    repository_commmits_not_found: 'No commit history was found for this repository',
    default: 'Something went wrong, please try again later',
  }
}
